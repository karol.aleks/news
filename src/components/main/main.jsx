import React from 'react';
import { ThemeContext } from '../theme-context/theme-context';

const Main = ({ className, elems }) => {
  return (
    <ThemeContext.Consumer>
      {theme => (
        <ul style={theme} className={className}>
          {elems.map(({ title, urlToImage, description, url }, ind) => (
            <li className="new" key={title[0] + ind}>
              <a href={url}>
                <h2 className="title">{title}</h2>
                <img className="image" src={urlToImage} alt={title} />
              </a>
              <p className="description">{description}</p>
            </li>
          ))}
        </ul>
      )}
    </ThemeContext.Consumer>
  );
};

export default Main;
