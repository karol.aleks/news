import React, { Component } from 'react';
import './app.css';
import Header from '../header';
import Main from '../main';
import { ThemeContext, themes } from '../theme-context/theme-context';

class App extends Component {
  state = {
    newsItems: [],
    theme: themes.light,
  };

  toggleTheme = () => {
    this.setState(state => ({
      theme: state.theme === themes.dark ? themes.light : themes.dark,
    }));
  };

  componentDidMount() {
    fetch(
      'https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=2c4a393166c146ac9800dba20ee6f91a',
    )
      .then(response => response.json())
      .then(res => {
        return res.articles;
      })
      .then(articles => {
        this.setState({
          newsItems: [...this.state.newsItems, ...articles],
        });
      })
      .catch(error => console.log(error));
  }

  render() {
    const { newsItems, theme } = this.state;

    return (
      <ThemeContext.Provider value={theme}>
        <>
          <Header changeTheme={this.toggleTheme} />
          <Main className="news" elems={newsItems} />
        </>
      </ThemeContext.Provider>
    );
  }
}

export default App;
