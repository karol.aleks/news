import React from 'react';
import { ThemeContext } from '../theme-context/theme-context';

const Header = props => {
  return (
    <ThemeContext.Consumer>
      {theme => (
        <header style={theme}>
          <h1 className="heading">Петушиный вестник</h1>
          <button style={theme} onClick={props.changeTheme} className="btn">
            Смени тему
          </button>
        </header>
      )}
    </ThemeContext.Consumer>
  );
};

export default Header;
